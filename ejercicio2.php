<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Usando metodo GET</title>

	<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.css">

	<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
<h2>Convercion Decimal a Binario</h2>

<form class="form-inline" name="formPost" method="GET" action="">

	<label>Escriba un decimal</label>
	<input type="text" class="form-control" name="txtnumero">
	<input type="submit" class="btn btn-primary" name="btnEnviar">

</form>
</div>
<?php 

if (!empty($_GET['txtnumero'])) {
	
	$numdecimal= $_GET['txtnumero'];

	echo "El numero decimal es: ". $numdecimal."<br>";

	$numbinario='';

	do{

		$numbinario=$numdecimal % 2 . $numbinario;
		$numdecimal=(int)($numdecimal/2);
	}while ($numdecimal>0);

	
		echo "Numero Binario: " . $numbinario;
	

}
 ?>
</body>
</html>
